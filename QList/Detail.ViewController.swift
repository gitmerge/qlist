//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Contacts
import Foundation
import MapKit
import SafariServices
import UIKit

class DetailViewController: UIViewController {
    convenience init() {
        self.init(for: nil)
    }

    init(for merchantId: Int?) {
        super.init(nibName: nil, bundle: nil)
        self.merchantId = merchantId
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - ViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        view.tintColor = UIView.appearance().tintColor

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(rightBarButtonItemTapped))

        configureDataProvider()
        configureViews()

        reloadData()
    }

    @objc
    func rightBarButtonItemTapped() {
        dismiss(animated: true)
    }

    @objc
    private func leftBarButtonLinkItemTapped() {
        if let link = dataProvider.detailItem?.links?.first, let url = URL(string: link.href) {
            navigationController?.pushViewController(SFSafariViewController(url: url), animated: true)
        }
    }

    @objc
    private func leftBarButtonPhoneNumberItemTapped() {
        if let phoneNumber = dataProvider.detailItem?.phoneNumber, let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }

    @objc
    private func leftBarButtonMapItemTapped() {
        if let placeMark = mapView.annotations.first as? MKPlacemark {
            MKMapItem(placemark: placeMark).openInMaps()
        }
    }

    // MARK: - Data

    var merchantId: Int? {
        didSet {
            if view.superview != nil {
                reloadData()
            }
        }
    }

    /**
     Binding for view model
     */
    private let dataProvider = MerchantDetail.DataProvider()

    private func configureDataProvider() {
        _ = dataProvider.$detailItem.receive(on: DispatchQueue.main).sink { [weak self] item in
            if let item = item {
                self?.renderData(for: item)
            }
        }
    }

    func reloadData() {
        if let merchantId = merchantId {
            dataProvider.fetch(for: merchantId)
        }
    }

    /// renderData
    ///
    /// - Parameter item:
    func renderData(for item: MerchantDetail) {
        // title
        navigationItem.title = ""

        // Image
        let imageUrl = itemImages.first?.asURL ?? Merchant.Image.DummyURL
        backgroundImageVIew.loader.loadImage(url: imageUrl)

        itemImageView.loader.loadImage(url: imageUrl) { [weak self] _ in
            self?.updateItemImageViewLayout()
        }

        // Rating
        if let ratings = item.reviewScore, let score = Float(ratings) {
            ratingView.isHidden = false
            ratingView.current = ratingView.max * CGFloat(score) / 10
        } else {
            ratingView.isHidden = true
        }

        // Text content
        var textStrings = [(title: String, description: String)]()

        textStrings.append((title: item.name ?? "", description: " "))

        // Contacts Info
        var leftBarButtonItems = [UIBarButtonItem]()
        if item.links?.first != nil {
            leftBarButtonItems.append(UIBarButtonItem(image: UIImage(systemName: "safari"), style: .plain, target: self, action: #selector(leftBarButtonLinkItemTapped)))
        }
        if item.phoneNumber != nil {
            leftBarButtonItems.append(UIBarButtonItem(image: UIImage(systemName: "phone.circle"), style: .plain, target: self, action: #selector(leftBarButtonPhoneNumberItemTapped)))
        }

        // Tags
        if let tags = item.tagGroups?.reduce([String](), { $0 + ($1.tags?.map { $0.name } ?? []) }) {
            textStrings.append((title: "Tags", description: tags.joined(separator: ", ")))
        }

        // Locale info
        if let locale = item.locale {
            let l = Locale(identifier: locale)

            if let regionCode = l.regionCode, let regionName = l.localizedString(forRegionCode: regionCode) {
                var regionDesc = regionName
                if let currency = item.currency, let currencyName = l.localizedString(forCurrencyCode: currency) {
                    regionDesc += ", \(currencyName.localizedCapitalized)"
                }
                if let timeZone = item.timezone {
                    regionDesc += ", \(timeZone)"
                }
                textStrings.append((title: "Location", description: regionDesc))
            }
        }

        // Location
        if let location = item.location, let locationCoordinates = location.coordinates {
            attachMapView()

            mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(leftBarButtonMapItemTapped)))

            let c = CLLocationCoordinate2D(latitude: locationCoordinates.latitude, longitude: locationCoordinates.longitude)
            let region = MKCoordinateRegion(center: c, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(region, animated: true)
            mapView.annotations.forEach(mapView.removeAnnotation)
            let place = MKPlacemark(coordinate: c, addressDictionary: [
                CNPostalAddressCountryKey: location.address?.country as Any,
                CNPostalAddressCityKey: location.address?.city as Any,
                CNPostalAddressStreetKey: location.address?.street as Any,
                CNPostalAddressPostalCodeKey: location.address?.zipcode as Any,
            ])
            mapView.addAnnotation(place)

            leftBarButtonItems.append(UIBarButtonItem(image: UIImage(systemName: "map"), style: .plain, target: self, action: #selector(leftBarButtonMapItemTapped)))
        }

        // Render text
        let bottomTextString = NSMutableAttributedString()
        for (i, textItem) in textStrings.enumerated() where textItem.title.count > 0 {
            let titleStyle = NSMutableParagraphStyle()
            titleStyle.lineSpacing = UIFont.labelFontSize / 4

            let title = NSMutableAttributedString(string: textItem.title + (textItem.description.count > 0 ? "\n" : ""))
            title.setAttributes([
                .font: UIFont.boldSystemFont(ofSize: UIFont.labelFontSize * 1.2),
                .foregroundColor: UIColor.label,
                .paragraphStyle: titleStyle,
            ], range: NSRange(title.string.startIndex ..< title.string.endIndex, in: title.string))

            bottomTextString.append(title)

            if textItem.description.count > 0 {
                let descriptionStyle = NSMutableParagraphStyle()
                descriptionStyle.alignment = .justified
                descriptionStyle.lineBreakMode = .byWordWrapping

                let description = NSMutableAttributedString(string: textItem.description + (i < textStrings.count - 1 ? "\n\n" : ""))
                description.setAttributes([
                    .font: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                    .foregroundColor: UIColor.label,
                    .paragraphStyle: descriptionStyle,
                ], range: NSRange(description.string.startIndex ..< description.string.endIndex, in: description.string))
                bottomTextString.append(description)
            }
        }

        bottomTextView.attributedText = bottomTextString
        bottomTextView.sizeToFit()
        bottomTextViewHeightAnchor.constant = bottomTextView.contentSize.height

        navigationItem.leftBarButtonItems = leftBarButtonItems.count > 0 ? leftBarButtonItems : nil
    }

    // MARK: - Views

    private lazy var containerView = UIView()

    private lazy var mapView: MKMapView = MKMapView(frame: .zero)

    private lazy var backgroundImageVIew: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        return view
    }()

    private lazy var itemImageViewHeightAnchor = itemImageView.heightAnchor.constraint(equalToConstant: 0)
    private lazy var itemImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 5.0
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 3, height: 4)
        view.layer.masksToBounds = true
        return view
    }()

    private func updateItemImageViewLayout() {
        let imageSize = itemImageView.image?.size ?? CGSize.zero
        let viewWidth = itemImageView.safeAreaLayoutGuide.layoutFrame.width
        itemImageViewHeightAnchor.constant = imageSize == .zero ? 0 : viewWidth * imageSize.height / imageSize.width

        itemImageCollectionView.reloadData()
        if itemImageCollectionView.dataSource?.collectionView(itemImageCollectionView, numberOfItemsInSection: 0) ?? 0 > 0 {
            itemImageCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .left)
        }
    }

    private lazy var itemImageCollectionView = UICollectionView(frame: .zero, collectionViewLayout: itemImageCollectionViewLayout)
    private lazy var itemImageCollectionViewLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()

    private lazy var bottomTextViewHeightAnchor = bottomTextView.heightAnchor.constraint(equalToConstant: 0)
    private lazy var bottomTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = nil
        textView.isUserInteractionEnabled = false
        textView.textContainer.maximumNumberOfLines = 0
        textView.textContainer.lineBreakMode = .byWordWrapping
        return textView
    }()

    private let paddingWhenConfigureViews: CGFloat = 20

    private lazy var ratingView = { () -> StarRatingView in
        let starView = StarRatingView()
        let attribute = StarRatingAttribute(type: .rate,
                                            point: 20,
                                            spacing: 2,
                                            emptyColor: view.tintColor,
                                            fillColor: view.tintColor,
                                            emptyImage: UIImage(systemName: "star")?.withTintColor(view.tintColor),
                                            fillImage: UIImage(systemName: "star.fill")?.withTintColor(view.tintColor))

        starView.configure(attribute, current: 0, max: 4)
        starView.isHidden = true
        return starView
    }()

    func configureViews() {
        view.addSubview(backgroundImageVIew)
        backgroundImageVIew.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageVIew.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImageVIew.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundImageVIew.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageVIew.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .systemUltraThinMaterial))
        backgroundImageVIew.addSubview(blurView)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.widthAnchor.constraint(equalTo: backgroundImageVIew.widthAnchor).isActive = true
        blurView.heightAnchor.constraint(equalTo: backgroundImageVIew.heightAnchor).isActive = true

        let contentView = backgroundImageVIew

        contentView.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leadingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: paddingWhenConfigureViews).isActive = true
        containerView.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor).isActive = true

        containerView.addSubview(itemImageView)
        itemImageView.isUserInteractionEnabled = true
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        itemImageView.topAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor).isActive = true
        itemImageView.heightAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.heightAnchor, multiplier: 0.3).isActive = true
        itemImageView.leadingAnchor.constraint(equalTo: backgroundImageVIew.leadingAnchor).isActive = true
        itemImageView.trailingAnchor.constraint(equalTo: backgroundImageVIew.trailingAnchor).isActive = true
        itemImageViewHeightAnchor.isActive = true

        itemImageView.addSubview(itemImageCollectionView)
        itemImageCollectionView.backgroundColor = .clear
        itemImageCollectionView.translatesAutoresizingMaskIntoConstraints = false
        itemImageCollectionView.leadingAnchor.constraint(equalTo: itemImageView.safeAreaLayoutGuide.leadingAnchor, constant: paddingWhenConfigureViews).isActive = true
        itemImageCollectionView.trailingAnchor.constraint(equalTo: itemImageView.safeAreaLayoutGuide.trailingAnchor, constant: -paddingWhenConfigureViews).isActive = true

        itemImageCollectionView.bottomAnchor.constraint(equalTo: itemImageView.safeAreaLayoutGuide.bottomAnchor, constant: -paddingWhenConfigureViews / 2).isActive = true
        itemImageCollectionView.heightAnchor.constraint(equalTo: itemImageView.safeAreaLayoutGuide.heightAnchor, multiplier: 0.2).isActive = true
        itemImageCollectionView.delegate = self
        itemImageCollectionView.dataSource = self
        itemImageCollectionView.register(ItemsImageCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: ItemsImageCollectionViewCell.self))
        itemImageCollectionView.allowsMultipleSelection = false
        itemImageCollectionView.showsVerticalScrollIndicator = false
        itemImageCollectionView.showsHorizontalScrollIndicator = false

        itemImageView.addSubview(ratingView)
        ratingView.translatesAutoresizingMaskIntoConstraints = false
        ratingView.widthAnchor.constraint(equalToConstant: ratingView.frame.size.width).isActive = true
        ratingView.heightAnchor.constraint(equalToConstant: ratingView.frame.size.height).isActive = true
        ratingView.trailingAnchor.constraint(equalTo: itemImageView.trailingAnchor, constant: -paddingWhenConfigureViews).isActive = true
        ratingView.topAnchor.constraint(equalTo: itemImageView.topAnchor, constant: paddingWhenConfigureViews / 2).isActive = true

        containerView.addSubview(bottomTextView)
        bottomTextView.translatesAutoresizingMaskIntoConstraints = false
        bottomTextView.topAnchor.constraint(equalToSystemSpacingBelow: itemImageView.bottomAnchor, multiplier: 1).isActive = true
        bottomTextView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: paddingWhenConfigureViews).isActive = true
        bottomTextView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -paddingWhenConfigureViews).isActive = true
        bottomTextViewHeightAnchor.isActive = true
    }

    private func attachMapView() {
        containerView.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: paddingWhenConfigureViews).isActive = true
        mapView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -paddingWhenConfigureViews).isActive = true
        mapView.topAnchor.constraint(equalTo: bottomTextView.bottomAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        updateItemImageViewLayout()
        itemImageCollectionView.collectionViewLayout.invalidateLayout()
    }
}

extension DetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedImageUrl = itemImages[safe: indexPath.row]?.asURL, itemImageView.loader.loadedImageURL != selectedImageUrl {
            itemImageView.loader.loadImage(url: selectedImageUrl)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout _: UICollectionViewLayout, sizeForItemAt _: IndexPath) -> CGSize {
        let size = ceil(collectionView.safeAreaLayoutGuide.layoutFrame.height)
        return CGSize(width: size * 1.5, height: size)
    }

    func collectionView(_: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? ItemsImageCollectionViewCell else { return }

        let item = itemImages[safe: indexPath.row]

        let imageUrl = item?.asURL ?? Merchant.Image.DummyURL
        cell.contentView.layer.removeAllAnimations()
        cell.contentView.alpha = 0
        cell.imageLoader.loadImage(url: imageUrl) { _ in
            UIView.animate(withDuration: 0.2, delay: 0, options: [UIView.AnimationOptions.beginFromCurrentState], animations: ({
                cell.contentView.alpha = 1
            }), completion: nil)
        }
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, insetForSectionAt _: Int) -> UIEdgeInsets {
        .zero
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumLineSpacingForSectionAt _: Int) -> CGFloat {
        5
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumInteritemSpacingForSectionAt _: Int) -> CGFloat {
        0
    }
}

extension DetailViewController: UICollectionViewDataSource {
    var itemImages: [Merchant.Image] {
        dataProvider.detailItem?.images ?? [Merchant.Image(url: Merchant.Image.DummyURL.absoluteString)]
    }

    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        itemImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemsImageCollectionViewCell.self), for: indexPath) as? ItemsImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.tintColor = view.tintColor
        cell.imageLoader.cancelLoadingImage()
        return cell
    }
}
