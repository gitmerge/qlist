//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation

extension APIClient {
    struct Merchant {}
}

extension APIClient.Merchant {
    enum Collection: Hashable {
        /// Get a full list
        case all

        /// Fetch searched results by provided keyword.
        case searched(keyword: String)
    }

    /// Fetch items in a page
    ///
    /// - Parameters:
    ///   - collection:
    ///   - page:
    ///   - completion:
    func fetch(where collection: Collection, pageIndex: Int = 0, per limit: Int = 30, completion: @escaping (MerchantResult?, Error?) -> Swift.Void) {
        let resource: APIClient.URLResource

        switch collection {
        case .all:
            resource = APIClient.URLResource(path: "merchants", method: "GET", queries: [
                URLQueryItem(name: "offset", value: String(pageIndex)),
                URLQueryItem(name: "limit", value: String(limit)),
            ])

        case let .searched(keyword):
            var queries = [
                URLQueryItem(name: "offset", value: String(pageIndex)),
                URLQueryItem(name: "limit", value: String(limit)),
            ]
            if keyword.count > 0 {
                queries.append(URLQueryItem(name: "query", value: keyword))
            }
            resource = APIClient.URLResource(path: "merchants", method: "GET", queries: queries)
        }

        let task = APIClient.URLSessionDataTask(for: resource, completion: completion)
        task?.resume()
    }

    /// Fetch detail items with its id
    ///
    /// - Parameters:
    ///   - for: merchants' Id
    ///   - completion: returns MerchantDetailItem
    func fetch(for merchantId: Int, completion: @escaping (MerchantDetail?, Error?) -> Swift.Void) {
        let resource = APIClient.URLResource(path: "merchants/\(merchantId)", method: "GET", queries: nil)
        let task = APIClient.URLSessionDataTask(for: resource, completion: completion)
        task?.resume()
    }
}

// https://api.quandoo.com/v1/merchants?offset=0&limit=30&query=Zim
