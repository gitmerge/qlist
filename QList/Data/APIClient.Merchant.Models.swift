//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation

/// A group as a page
struct MerchantResult: Codable {
    let offset: Int
    let size: Int
    let limit: Int
    let merchants: [Merchant]?
}

/// the merchant element discovered
struct Merchant: Codable, Equatable {
    let id: Int
    let name: String?
    let phoneNumber: String?
    let currency: String?
    let locale: String?
    let timezone: String?

    let location: Location?
    struct Location: Codable {
        let coordinates: Coordinate?
        let address: Address?

        struct Coordinate: Codable {
            let latitude: Double
            let longitude: Double
        }

        struct Address: Codable {
            let street: String?
            let number: String?
            let zipcode: String?
            let city: String?
            let country: String?
        }
    }

    let reviewScore: String?

    let tagGroups: [TagGroup]?
    struct TagGroup: Codable {
        let type: String
        let tags: [Tag]?

        struct Tag: Codable {
            let id: String
            let name: String
        }
    }

    let images: [Image]?
    struct Image: Codable {
        let url: String
    }

    let links: [Link]?
    struct Link: Codable {
        let href: String
        let method: String
        let rel: String?
    }

    let bookable: Bool

    let openingTimes: OpeningTimes?
    struct OpeningTimes: Codable {
        let standardOpeningTimes: Times?

        struct Times: Codable {
            struct TimeRange: Codable {
                let start: String
                let end: String
            }

            let MONDAY: [TimeRange]?
            let TUESDAY: [TimeRange]?
            let WEDNESDAY: [TimeRange]?
            let THURSDAY: [TimeRange]?
            let FRIDAY: [TimeRange]?
            let SATURDAY: [TimeRange]?
            let SUNDAY: [TimeRange]?
        }
    }

    let ccvEnabled: Bool

    static func == (lhs: Merchant, rhs: Merchant) -> Bool {
        lhs.id == rhs.id
    }
}

extension Merchant.Image {
    static let DummyURL = URL(string: "https://scontent-ber1-1.xx.fbcdn.net/v/t1.0-9/51604174_2439515592789625_4385030217576480768_n.png?_nc_cat=104&_nc_oc=AQmZDZsVJJozOHWw2SLdb6KKKiQSQ-QyymaAel4n41lQDK-LKnuOVVUlYreFgTtaWxc&_nc_ht=scontent-ber1-1.xx&oh=8412b602acb1743ef972b51102721097&oe=5E554007")!

    var asURL: URL? { URL(string: url) }
}

/// the merchant detail element discovered
struct MerchantDetail: Codable {
    let id: Int
    let name: String?
    let phoneNumber: String?
    let currency: String?
    let locale: String?
    let timezone: String?
    let location: Merchant.Location?
    let reviewScore: String?
    let tagGroups: [Merchant.TagGroup]?
    let images: [Merchant.Image]?
    let links: [Merchant.Link]?
    let bookable: Bool
    let openingTimes: Merchant.OpeningTimes?
    let ccvEnabled: Bool
}
