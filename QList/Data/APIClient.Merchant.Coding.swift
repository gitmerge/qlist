//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation

extension APIClient.Merchant.Collection: Codable {
    enum Key: CodingKey {
        case rawValue
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        switch try? container.decode(Int.self, forKey: .rawValue) {
        case 0:
            self = .all
        default:
            if let rawStringValue = try? container.decode(String.self, forKey: .rawValue) {
                self = .searched(keyword: rawStringValue)
            } else {
                throw CodingError.unknownValue
            }
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        switch self {
        case .all:
            try container.encode(0, forKey: .rawValue)
        case let .searched(keyword):
            try container.encode(keyword, forKey: .rawValue)
        }
    }
}
