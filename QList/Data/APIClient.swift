//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation

struct APIClient {
    private static let Version = "v1"
    private static let BaseURI = URL(string: "https://api.quandoo.com/\(Version)")!

    /// Resource provides a url request
    struct URLResource {
        let path: String
        let method: String
        var queries: [URLQueryItem]?

        var urlRequest: URLRequest? {
            guard let url = url else {
                return nil
            }

            var urlRequest = URLRequest(url: url)
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(Locale.current.languageCode, forHTTPHeaderField: "Accept-Language")
            urlRequest.httpMethod = method
            return urlRequest
        }

        private var url: URL? {
            let baseURL = BaseURI.appendingPathComponent(path)
            var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)
            urlComponents?.queryItems = queries
            return urlComponents?.url
        }
    }

    /// Response error object
    struct URLResponseError: Decodable {
        let errorType: String?
        let errorMessage: String?
    }
}
