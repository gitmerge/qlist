//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation

private extension HTTPURLResponse {
    var hasSuccessStatusCode: Bool {
        200 ... 299 ~= statusCode
    }
}

extension APIClient {
    /// Making URLSessionDataTask instance for json object from resource.
    ///
    /// - Parameters:
    ///   - resource:
    ///   - session:
    ///   - completion:
    /// - Returns:
    static func URLSessionDataTask<T: Decodable>(for resource: APIClient.URLResource, session: URLSession = URLSession.shared, completion: @escaping (T?, Error?) -> Swift.Void) -> URLSessionDataTask? {
        guard let request = resource.urlRequest else {
            completion(nil, "URL is wrong.")
            return nil
        }

        return session.dataTaskOnDiskCacheFallback(with: request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let error = error {
                completion(nil, error.localizedDescription)
                return
            }

            guard let data = data else {
                completion(nil, "Not found resource")
                return
            }

            let decoder = JSONDecoder()

            guard (response as? HTTPURLResponse)?.hasSuccessStatusCode ?? false else {
                let message = try? decoder.decode(APIClient.URLResponseError.self, from: data)
                completion(nil, message?.errorMessage ?? "\(URLResponseError.self) is unable to decode: \(String(data: data, encoding: .utf8) ?? "")")
                return
            }

            do {
                let page = try decoder.decode(T.self, from: data)
                completion(page, nil)

            } catch let e {
                completion(nil, e.localizedDescription + "\n\n\(String(data: data, encoding: .utf8) ?? "")")
            }
        }
    }
}
