//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import UIKit

class ItemsCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()

    override var isSelected: Bool {
        didSet {
            contentView.layer.borderColor = tintColor.cgColor
            contentView.layer.borderWidth = isSelected ? 2 : 0
        }
    }

    private let labelDimmingView = UIVisualEffectView(effect: UIBlurEffect(style: .systemUltraThinMaterial))

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        label.textColor = .label
        return label
    }()

    var titleLabelText: String? {
        didSet {
            titleLabel.text = titleLabelText
            labelDimmingView.isHidden = titleLabelText?.count ?? 0 == 0
        }
    }

    var imageLoader: UIImageLoader {
        imageView.loader
    }

    private func initialize() {
        contentView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true

        let padding = titleLabel.font.lineHeight / 2

        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -padding).isActive = true

        contentView.insertSubview(labelDimmingView, belowSubview: titleLabel)
        labelDimmingView.isUserInteractionEnabled = false
        labelDimmingView.translatesAutoresizingMaskIntoConstraints = false
        labelDimmingView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        labelDimmingView.topAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -padding).isActive = true
        labelDimmingView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        imageView.loader.cancelLoadingImage()
        imageView.image = nil
    }
}
