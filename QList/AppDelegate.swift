//
//  AppDelegate.swift
//  QList
//
//  Created by Taeho Lee on 11/1/19.
//  Copyright © 2019 Quandoo GmbH. All rights reserved.
//

import Foundation
import UIKit

@UIApplicationMain
/// Coding challenge
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIView.appearance().tintColor = .systemYellow
        UIImageLoader.memoryCachesTotalCostLimit = Int(Double(ProcessInfo.processInfo.physicalMemory) * 0.2)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.systemBackground
        window?.rootViewController = UINavigationController(rootViewController: MainViewController())
        window?.makeKeyAndVisible()
        return true
    }
}
