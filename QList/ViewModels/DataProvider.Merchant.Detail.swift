//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Combine
import Foundation

extension MerchantDetail {
    class DataProvider: ObservableObject {
        /// A property to subscribe currencies
        @Published private(set) var detailItem: MerchantDetail?

        private let client = APIClient.Merchant()

        /// fetch details for merchant
        ///
        /// - Parameter id: merchantId
        func fetch(for merchantId: Int) {
            client.fetch(for: merchantId) { item, _ in
                self.detailItem = item
            }
        }
    }
}
