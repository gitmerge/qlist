//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Combine
import Foundation

extension Merchant {
    final class DataProvider: ObservableObject {
        // MARK: - Data Handling

        /// Indexes after fetching items
        @Published private(set) var fetchedItemIndexes = [Int]()

        /// Current whole items
        var fetchedItems: [Merchant] { itemsByCollection[collection]?.items ?? [] }

        /// Current totalNumberOfItems
        var totalNumberOfItems: Int { itemsByCollection[collection]?.total ?? 0 }

        /// Current Page
        var targetPageIndex: Int = 0

        /// Dictionary
        struct CollectionItem: Codable {
            let quota: Int
            let total: Int
            let pageIndex: Int
            let items: [Merchant]
        }

        private var itemsByCollection = [APIClient.Merchant.Collection: CollectionItem]() {
            didSet {
                if targetPageIndex > 0 {
                    // Set newly appended item indexes.
                    let newItemsCount = itemsByCollection[collection]?.quota ?? 0
                    let startIndex = fetchedItems.count - newItemsCount
                    let endIndex = startIndex + newItemsCount
                    fetchedItemIndexes = Array(startIndex ..< endIndex)
                } else {
                    fetchedItemIndexes = Array(0 ..< fetchedItems.count)
                }
            }
        }

        private var collection: APIClient.Merchant.Collection = .all {
            didSet {
                targetPageIndex = 0
                fetchItems()
            }
        }

        /// Keyword to filter searched results
        var searchingKeyword: String? {
            didSet {
                if let k = searchingKeyword, k.count > 0 {
                    collection = .searched(keyword: k)

                } else {
                    collection = .all
                }
            }
        }

        var itemsPerPage: Int = 30 {
            didSet {
                fetchItems()
            }
        }

        // MARK: - APIClient

        private let client = APIClient.Merchant()

        /// Fetch from API, and appended items from results.
        func fetchItems(completion: (() -> Swift.Void)? = nil) {
            let collection = self.collection
            let targetPageIndex = self.targetPageIndex

            client.fetch(where: collection, pageIndex: targetPageIndex, per: itemsPerPage) { page, error in
                DispatchQueue.main.async {
                    guard error == nil else {
                        return
                    }
                    guard let page = page else {
                        return
                    }
                    guard let newItems = page.merchants else {
                        return
                    }
                    guard newItems.count > 0 else {
                        return
                    }

                    // Set configuration values.
                    var updatingItems = self.itemsByCollection[collection]?.items ?? []
                    updatingItems += newItems

                    let total = self.itemsByCollection[collection]?.total ?? page.size

                    self.itemsByCollection[collection] = CollectionItem(
                        quota: newItems.count,
                        total: total,
                        pageIndex: targetPageIndex,
                        items: updatingItems
                    )
                    completion?()
                }
            }
        }
    }
}
