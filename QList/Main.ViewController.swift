//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import UIKit

import UIKit

class MainViewController: UIViewController {
    private lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    fileprivate var timerForSearchingDelay: Timer?

    /**
     Binding for view model
     */
    /// Defined dataProvider as a controller + view model
    private let dataProvider = Merchant.DataProvider()

    private func configureDataProvider() {
        _ = dataProvider.$fetchedItemIndexes
            .receive(on: RunLoop.main)
            .sink { [weak self] appendedIndexes in
                print(appendedIndexes)
                self?.reloadTableView(at: appendedIndexes)
            }

        _fetch3PagesInitially()

        // regular
//        dataProvider.fetchedItems()
    }

    // For a requirement "On launch your app should fetch the first 120 merchants (with a 30 items/page limit)"
    private func _fetch3PagesInitially() {
        guard dataProvider.targetPageIndex < 3 else { return }

        dataProvider.fetchItems { [weak self] in
            self?.dataProvider.targetPageIndex += 1
            self?._fetch3PagesInitially()
        }
    }

    /**
     Configuration of view controller or navigation
     */
    private func configureViewController() {
        view.backgroundColor = .systemBackground
        view.tintColor = UIView.appearance().tintColor

        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.isTranslucent = true

        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false

        navigationItem.title = "Quandoo Restaurants"

        definesPresentationContext = true
    }

    /**
     Configuration of views elements
     */
    private func configureViews() {
        view.addSubview(collectionView)
        collectionView.backgroundColor = view.backgroundColor
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self

        collectionView.register(ItemsCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: ItemsCollectionViewCell.self))
        collectionView.allowsMultipleSelection = false
        collectionView.showsVerticalScrollIndicator = false
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        configureViewController()
        configureViews()
        configureDataProvider()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }
}

extension MainViewController: UISearchResultsUpdating, UISearchBarDelegate {
    ///
    ///
    /// - Parameter searchController:
    func updateSearchResults(for searchController: UISearchController) {
        var keyword = searchController.searchBar.text
        keyword = keyword?.count ?? 0 > 0 ? keyword : nil

        if dataProvider.searchingKeyword != keyword {
            if collectionView.dataSource?.collectionView(collectionView, numberOfItemsInSection: 0) ?? 0 > 0 {
                collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }

            timerForSearchingDelay?.invalidate()
            timerForSearchingDelay = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { [weak self] _ in
                self?.dataProvider.searchingKeyword = keyword
            }
        }
    }

    ///
    ///
    /// - Parameters:
    ///   - _:
    ///   - searchText:
    func searchBar(_: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            dataProvider.searchingKeyword = nil
        }
    }

    ///
    ///
    /// - Parameter _:
    func searchBarSearchButtonClicked(_: UISearchBar) {
        navigationItem.searchController?.dismiss(animated: false)
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .left)

        if let selectedItem = dataProvider.fetchedItems[safe: indexPath.row] {
            let detailViewController = DetailViewController(for: selectedItem.id)
            detailViewController.modalPresentationStyle = .currentContext
            present(UINavigationController(rootViewController: detailViewController), animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout _: UICollectionViewLayout, sizeForItemAt _: IndexPath) -> CGSize {
        let orientation: UIInterfaceOrientation = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.windowScene?.interfaceOrientation ?? .portrait
        let size = ceil(collectionView.safeAreaLayoutGuide.layoutFrame.width / (orientation.isPortrait ? 3 : 6))
        return CGSize(width: size, height: size / 1.2)
    }

    func collectionView(_: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? ItemsCollectionViewCell else { return }

        let item = dataProvider.fetchedItems[safe: indexPath.item]

        cell.imageLoader.cancelLoadingImage()

        let imageUrl = item?.images?.first?.asURL ?? Merchant.Image.DummyURL
        cell.contentView.layer.removeAllAnimations()
        cell.contentView.alpha = 0
        cell.imageLoader.loadImage(url: imageUrl) { _ in
            UIView.animate(withDuration: 0.2, delay: 0, options: [UIView.AnimationOptions.beginFromCurrentState], animations: ({
                cell.contentView.alpha = 1
            }), completion: nil)
        }
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, insetForSectionAt _: Int) -> UIEdgeInsets {
        .zero
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumLineSpacingForSectionAt _: Int) -> CGFloat {
        0
    }

    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, minimumInteritemSpacingForSectionAt _: Int) -> CGFloat {
        0
    }
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        dataProvider.totalNumberOfItems
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemsCollectionViewCell.self), for: indexPath) as? ItemsCollectionViewCell else {
            return UICollectionViewCell()
        }

        let item = dataProvider.fetchedItems[safe: indexPath.item]

        if cell.contentView.layer.borderWidth == 0 {
            cell.contentView.layer.borderWidth = 0.5
            cell.contentView.layer.borderColor = collectionView.backgroundColor?.cgColor
        }

        cell.tintColor = view.tintColor
        cell.titleLabelText = item?.name

        return cell
    }
}

extension MainViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            dataProvider.targetPageIndex += 1
            dataProvider.fetchItems()
        }
    }
}

private extension MainViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        indexPath.row >= dataProvider.fetchedItems.count
    }

    /// reloadTableView for indexes
    ///
    /// - Parameter appendedIndexes:
    func reloadTableView(at appendedIndexes: [Int]) {
        // first page
        if appendedIndexes[safe: 0] ?? 0 == 0 {
            collectionView.reloadData()

        } else {
            let appendedIndexPaths = appendedIndexes.map { IndexPath(row: $0, section: 0) }
            let reloadingIndexPaths = Array(Set(collectionView.indexPathsForVisibleItems).intersection(appendedIndexPaths))

            collectionView.reloadItems(at: reloadingIndexPaths)
        }
    }
}
