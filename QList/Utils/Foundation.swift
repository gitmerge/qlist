//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation
import UIKit

/// Simplify to make error
extension String: Error {}

extension Collection {
    /// Returns nil if index of value was out of bound
    ///
    /// - Parameter index:
    /// - Returns:
    subscript(safe index: Index) -> Element? {
        indices.contains(index) ? self[index] : nil
    }
}
