//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation

/// An extension of the disk cache for offline state
extension URLSession {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void

    /// Create dataTask On Disk Cache Fallback
    ///
    /// - Parameters:
    ///   - request: URLRequest
    ///   - completionHandler:
    /// - Returns:
    func dataTaskOnDiskCacheFallback(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let sessions = makeCachedSession()
        return sessions.network.dataTask(with: request) { networkData, networkResponse, networkError in
            guard networkError == nil else {
                sessions.local.dataTask(with: request, completionHandler: completionHandler).resume()
                return
            }
            completionHandler(networkData, networkResponse, networkError)
        }
    }

    /// Create dataTask On Disk Cache Fallback
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - completionHandler:
    /// - Returns:
    func dataTaskOnDiskCacheFallback(with url: URL, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let sessions = makeCachedSession()
        return sessions.network.dataTask(with: url) { networkData, networkResponse, networkError in
            guard networkError == nil else {
                sessions.local.dataTask(with: url, completionHandler: completionHandler).resume()
                return
            }
            completionHandler(networkData, networkResponse, networkError)
        }
    }

    private func makeCachedSession() -> (local: URLSession, network: URLSession) {
        // for implementing loadFromRemoteElseUseCache via reloadIgnoringLocalCacheData + returnCacheDataDontLoad ==
        let networkSession = makeSession(withPolicy: .reloadIgnoringLocalCacheData)
        let localSession = makeSession(withPolicy: .returnCacheDataDontLoad)
        return (local: localSession, network: networkSession)
    }

    private static let URLCacheOnDisk = URLCache(memoryCapacity: 0, diskCapacity: 100 * 1024 * 1024, diskPath: #function)

    private func makeSession(withPolicy policy: NSURLRequest.CachePolicy, timeout _: TimeInterval = 5) -> URLSession {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = policy
        config.timeoutIntervalForRequest = 5
        config.urlCache = type(of: self).URLCacheOnDisk

        return URLSession(configuration: config)
    }
}
