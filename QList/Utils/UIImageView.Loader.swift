//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Combine
import Foundation
import UIKit

/// UIImageView extension for using simply UIImageLoader
extension UIImageView: UIImageRepresentable {
    private static var LoaderKey: Int?

    var loader: UIImageLoader {
        var loader = objc_getAssociatedObject(self, &type(of: self).LoaderKey)
        if loader == nil {
            loader = UIImageLoader(representer: self)
            objc_setAssociatedObject(self, &type(of: self).LoaderKey, loader, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        return loader as! UIImageLoader
    }
}
