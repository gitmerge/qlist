//
// Created by Taeho Lee on 11/4/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

import Foundation
import UIKit

/// a protocol for DI
protocol UIImageRepresentable: AnyObject {
    var image: UIImage? { set get }
}

/// ImageViewLoader is a wrapper handler with weak image view instance + static cache.
class UIImageLoader {
    /// Static cache.
    private static let memoryCache = NSCache<NSURL, UIImage>()

    static var memoryCachesTotalCostLimit: Int {
        set { memoryCache.totalCostLimit = newValue }
        get { memoryCache.totalCostLimit }
    }

    /// A private queue for decoding images
    private let decodingQueue = DispatchQueue(label: #file, qos: .background, attributes: .concurrent)

    private var imageDownloadTask: URLSessionDataTask?

    /// External image view as a week ref.
    private weak var imageRepresenter: UIImageRepresentable?

    /// Enable cache
    var enableMemoryCache: Bool

    /// To track image load state + get actual url of loaded image
    @Published private(set) var loadedImageURL: URL?

    init(representer: UIImageRepresentable?, enableMemoryCache: Bool = true) {
        imageRepresenter = representer
        self.enableMemoryCache = enableMemoryCache
    }

    func cachedImage(url: URL) -> UIImage? {
        enableMemoryCache ? type(of: self).memoryCache.object(forKey: url as NSURL) : nil
    }

    func cancelLoadingImage() {
        imageDownloadTask?.cancel()
    }

    /// load image
    ///
    /// - Parameters:
    ///   - url:
    ///   - urlDegraded: a url for prefetching degraded image
    ///   - completion:
    func loadImage(url: URL, urlDegraded: URL? = nil, completion: ((Bool) -> Swift.Void)? = nil) {
        let imageCache = type(of: self).memoryCache
        let completionQueue = DispatchQueue.main

        // NSCache is already thread-safe.
        let cacheKey = url
        if let cachedImage = cachedImage(url: url) {
            completionQueue.async { [weak self] in
                self?.imageRepresenter?.image = cachedImage
                completion?(true)
            }
            return
        }

        loadedImageURL = nil

        // Capturing values
        let wasRequestedForDegraded = urlDegraded != nil
        let cacheEnabled = enableMemoryCache
        let imageView = imageRepresenter

        // Fetch from remote
        imageDownloadTask = URLSession.shared.dataTaskOnDiskCacheFallback(with: urlDegraded ?? url) { [weak self] data, response, error in
            if error != nil {
                completionQueue.async { completion?(false) }
                return
            }

            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completionQueue.async { completion?(false) }
                return
            }

            guard let data = data else {
                completionQueue.async { completion?(false) }
                return
            }

            self?.decodingQueue.async {
                // decode
                guard let image = UIImage(data: data) else {
                    if wasRequestedForDegraded {
                        // Retry with original url
                        self?.loadImage(url: url, urlDegraded: nil, completion: completion)
                    } else {
                        completionQueue.async { completion?(false) }
                    }
                    return
                }

                // Render image
                completionQueue.async {
                    imageView?.image = image

                    if !wasRequestedForDegraded {
                        self?.loadedImageURL = url
                    }
                }

                // if previous request was a url for degraded result, request again with main url
                if wasRequestedForDegraded {
                    self?.loadImage(url: url, urlDegraded: nil, completion: completion)

                } else {
                    // Put image into cache
                    if cacheEnabled {
                        imageCache.setObject(image, forKey: cacheKey as NSURL)
                    }

                    // Final completion
                    completionQueue.async {
                        completion?(true)
                    }
                }
            }
        }
        imageDownloadTask?.resume()
    }
}
