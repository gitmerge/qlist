# Requirements
- iOS 13 + Swift 5.1 + Xcode 11.0(11A420a) or later

# Approaches
- iPhone/iPad/orientations/dark theme support
- Full auto layout
- Infinite scrolling
- Offline support using disk cache in URLSession
- Realtime search result presenting by query parameter
- Used Combine for viewmodel-view binding
- Tests Target: QListTests
