//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

@testable import QList
import XCTest

class ViewModelsTests: XCTestCase {
    func testFetchItemsByPage() {
        let dataProvider = Merchant.DataProvider()
        let e = expectation(description: "page operations should be completed without an error.")

        _ = dataProvider.$fetchedItemIndexes.receive(on: DispatchQueue.main).sink { appendedIndexes in
            if appendedIndexes.count == 0 {
                return
            }
            XCTAssertTrue(dataProvider.totalNumberOfItems >= appendedIndexes.count)
        }

        dataProvider.targetPageIndex = 0

        DispatchQueue.global().async {
            let group = DispatchGroup()

            let pagesTest = 10

            var count = 0

            for _ in 0 ..< pagesTest {
                group.enter()
                dataProvider.targetPageIndex += 1

                let itemsCount = dataProvider.fetchedItems.count
                dataProvider.fetchItems {
                    print(dataProvider.fetchedItems.count)
                    XCTAssertTrue(dataProvider.fetchedItems.count > itemsCount)
                    count = dataProvider.fetchedItems.count
                    group.leave()
                }
            }

            group.notify(queue: .main) {
                XCTAssertTrue(dataProvider.fetchedItems.count == count)
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }

    func testFetchDetailItem() {
        let dataProvider = MerchantDetail.DataProvider()
        let e = expectation(description: "operations should be completed without an error.")

        _ = dataProvider.$detailItem.receive(on: DispatchQueue.main).sink { detailItem in
            if detailItem != nil {
                e.fulfill()
            }
        }

        dataProvider.fetch(for: 49599)

        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }
}
