//
// Created by Taeho Lee on 11/1/19.
// Copyright (c) 2019 Quandoo GmbH. All rights reserved.
//

@testable import QList
import XCTest

class UIImageViewLoaderTests: XCTestCase {
    let client = APIClient.Merchant()

    func testFetchingImages() {
        let e = expectation(description: "all image loading should be completed without an error.")

        let imageLoader = UIImageLoader(representer: UIImageView())

        DispatchQueue.global().async {
            self.client.fetch(where: .all) { page, error in
                XCTAssertNil(error, error?.localizedDescription ?? "")
                XCTAssertTrue(page != nil && page?.merchants?.count ?? 0 > 0)

                let group = DispatchGroup()
                for result in page?.merchants ?? [] {
                    group.enter()
                    if let imageUrl = result.images?.first?.url, let url = URL(string: imageUrl) {
                        print(url)
                        imageLoader.loadImage(url: url) { succeed in
                            XCTAssertTrue(succeed)
                            XCTAssertNotNil(imageLoader.cachedImage(url: url))
                            group.leave()
                        }
                    } else {
                        group.leave()
                    }
                }

                group.notify(queue: .main) {
                    e.fulfill()
                }
            }
        }

        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }
}
