//
//  Tests.APIClient.swift
//  QListTests
//
//  Created by Taeho Lee on 11/1/19.
//  Copyright © 2019 Quandoo GmbH. All rights reserved.
//

@testable import QList
import XCTest

class APIClientTests: XCTestCase {
    let client = APIClient.Merchant()

    func testFetchingByCollections() {
        let e = expectation(description: "a client would complete to return results without an error.")

        let targetCollections: [APIClient.Merchant.Collection] = [
            .all,
            .searched(keyword: "Hot"),
            .searched(keyword: ""),
        ]

        let group = DispatchGroup()

        DispatchQueue.global().async {
            for collection in targetCollections {
                group.enter()

                let limit = 30
                self.client.fetch(where: collection, per: limit) { page, error in
                    XCTAssertNotNil(page)
                    XCTAssertTrue(page?.merchants?.count ?? 0 > 0)
                    XCTAssertTrue(page?.merchants?.count ?? 0 <= limit)
                    XCTAssertNil(error, error?.localizedDescription ?? "")
                    group.leave()
                }
            }

            group.notify(queue: .main) {
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }

    func testFetchingInformation() {
        let e = expectation(description: "a client would complete to return results without an error.")

        client.fetch(for: 49599) { item, error in
            XCTAssertNil(error, error?.localizedDescription ?? "")
            XCTAssertNotNil(item)
            XCTAssertNotNil(item?.id)
            e.fulfill()
        }

        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }
}
